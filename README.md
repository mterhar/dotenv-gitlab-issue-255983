# Artifacts can be passed by ref, overwrite unfortunately

Make a project that uses [parent-child pipelines](https://docs.gitlab.com/ee/ci/parent_child_pipelines.html) to pass a [dotenv report](https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html#artifactsreportsdotenv) to a child pipeline. Works great.  Start two of the same pipeline, now they collide. 

Related to [GitLab Issue 255983](https://gitlab.com/gitlab-org/gitlab/-/issues/255983)

## Reproduction steps

### Without child pipeline

Go to the pipelines view. Trigger a pipeline with "CUSTOM_VARIABLE" set to something interesting. 

When that job is started, trigger the smae pipeline again with a different CUSTOM_VARIABLE set to something else. 

See how the first pipeline's output is bad...